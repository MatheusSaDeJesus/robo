package time;

import java.awt.Color;

import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.HitWallEvent;
import robocode.ScannedRobotEvent;

public class TesteRobo extends AdvancedRobot {

	@Override // LOOP ENQUANTO ROBO TA VIVO
	public void run() {
		
		boolean angulo = true;
		

		// CORES DO TANQUE
		this.setBodyColor(Color.BLACK); // CORPO = PRETO
		this.setGunColor(Color.BLACK); // ARMA = VERMELHO
		this.setRadarColor(Color.BLACK); // RADAR = VERMELHO
		this.setScanColor(Color.BLACK); // VARREDURA = PRETO
		this.setBulletColor(Color.RED); // BALA = VERMELHO

	while (true) {

		if(angulo == true) {
				for (int voltasDireitas = 0; voltasDireitas < 4; voltasDireitas++) {

					this.setAhead(45);
					this.setTurnRight(30);
				
				}
				for (int voltasEsquerda = 4; voltasEsquerda > 0; voltasEsquerda--) {

					this.setAhead(45);
					this.setTurnLeft(30);
				
				}
				angulo = false;
		}
		
		else {
			for (int voltasDireitas2 = 0; voltasDireitas2 < 2; voltasDireitas2++) {

				this.setAhead(30);
				this.setTurnRight(15);
			
			}
			for (int voltasEsquerda2 = 2; voltasEsquerda2 > 0; voltasEsquerda2--) {

				this.setAhead(30);
				this.setTurnLeft(15);
					
				}
			angulo = true;
			}
		}
	}

	@Override // ENCONTRAR ALGUEM
	public void onScannedRobot(ScannedRobotEvent event) {
		this.setBack(10);
		fire(1);
		execute();
		}

	// SE RECEBEU TIRO
	public void onHitByBullet(HitByBulletEvent e) {
		this.setTurnRight(15);
		back(10);
		execute();
	}

	// BATER NA PAREDE
	public void onHitWall(HitWallEvent e) {
		this.setAhead(100);
		this.setTurnLeft(45);
	}

}
