package SolutisRobo;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

public class MatheusSa extends Robot
{
	public void run() {
		setColors(Color.black,Color.black,Color.red);


		while(true) {
		//LOOP ENQUANTO O ROBO TA VIVO
			ahead(100); 
			turnGunRight(360);
			back(100);
			turnGunRight(360);
		}
	}
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like
		fire(1);
	}
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		back(10);
	}
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		back(20);
	}	
}
