package roboteste2;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

public class RoboTeste2 extends AdvancedRobot {

	// Direção do movimento: 1 = mover para frente, 0 = ficar parado, -1 = mover para trás
	int moveDirection;

	// Direção da curva: 1 = virar à direita, 0 = sem virar, -1 = virar à esquerda
	int turnDirection;

	// Quantidade de pixels / unidades para mover
	double moveAmount;

	// A coordenada do objetivo (x, y)
	int aimX, aimY;

	// Força de fogo, onde 0 = não dispara
	int firePower;

	// Chamado quando o robô deve ser executado
	public void run() {

		// Define as cores do robô
		// corpo = preto, arma = branco, radar = vermelho
		setColors(Color.BLACK, Color.BLACK, Color.BLACK);

		// Loop forever
		for (;;) {
			// Define o robô para se mover para frente, para trás ou parar de se mover, dependendo
			// na direção do movimento e quantidade de pixels para mover
			setAhead(moveAmount * moveDirection);

			// Diminui a quantidade de pixels a se mover até chegarmos a 0 pixels
			// Desta forma, o robô irá parar automaticamente se a roda do mouse
			// parou sua rotação
			moveAmount = Math.max(0, moveAmount - 1);

			// Define o robô para virar à direita ou à esquerda (na velocidade máxima) ou
			// pare de virar dependendo da direção da curva
			setTurnRight(45 * turnDirection); // degrees

			// Vira a arma em direção à coordenada de mira atual (x, y) controlada por
			// a coordenada atual do mouse
			double angle = normalAbsoluteAngle(Math.atan2(aimX - getX(), aimY - getY()));

			setTurnGunRightRadians(normalRelativeAngle(angle - getGunHeadingRadians()));

			// Dispare a arma com o poder de fogo especificado, a menos que o poder de fogo = 00
			if (firePower > 0) {
				setFire(firePower);
			}

			// Execute todas as instruções definidas pendentes
			execute();

			// A próxima curva é processada neste loop ..
		}
	}

	// Chamado quando uma tecla foi pressionada
	public void onKeyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case VK_UP:
		case VK_W:
			// Tecla de seta para cima: direção do movimento = para frente (infinitamente)
			moveDirection = 1;
			moveAmount = Double.POSITIVE_INFINITY;
			break;

		case VK_DOWN:
		case VK_S:
			// Tecla de seta para baixo: mover direção = para trás (infinitamente)
			moveDirection = -1;
			moveAmount = Double.POSITIVE_INFINITY;
			break;

		case VK_RIGHT:
		case VK_D:
			// Seta para a direita: virar direção = direita
			turnDirection = 1;
			break;

		case VK_LEFT:
		case VK_A:
			// Seta para a esquerda: virar direção = esquerda
			turnDirection = -1;
			break;
		}
	}

	// Chamado quando uma tecla é liberada (após ser pressionada)
	public void onKeyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
		case VK_UP:
		case VK_W:
		case VK_DOWN:
		case VK_S:
			// Teclas de seta para cima e para baixo: direção do movimento = ficar parado
			moveDirection = 0;
			moveAmount = 0;
			break;

		case VK_RIGHT:
		case VK_D:
		case VK_LEFT:
		case VK_A:
			// Teclas de seta para a direita e esquerda: virar direção = parar de virar
			turnDirection = 0;
			break;
		}
	}

	// Chamado quando a roda do mouse é girada
	public void onMouseWheelMoved(MouseWheelEvent e) {
		// Se a rotação da roda for negativa, significa que ela foi movida para frente.
		// Defina a direção do movimento = para frente, se a roda for movida para frente.
		// Caso contrário, defina a direção do movimento = para trás

		moveDirection = (e.getWheelRotation() < 0) ? 1 : -1;
		// Definir a quantidade de movimento = quantidade absoluta de rotação da roda * 5 (velocidade)
		// Aqui, 5 significa 5 pixels por etapa de rotação da roda. Quanto maior o valor, o
		// mais velocidade
		
		moveAmount += Math.abs(e.getWheelRotation()) * 5;
	}

	// Chamado quando o mouse é movido
	public void onMouseMoved(MouseEvent e) {
		// Definir a coordenada do objetivo = a coordenada do ponteiro do mouse
		aimX = e.getX();
		aimY = e.getY();
	}

	// Chamado quando um botão do mouse é pressionado
	public void onMousePressed(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON3) {
			// Botão 3: poder de fogo = 3 pontos de energia, cor do marcador = vermelho
			firePower = 3;
			setBulletColor(Color.RED);
		} else if (e.getButton() == MouseEvent.BUTTON2) {
			// Botão 2: poder de fogo = 2 pontos de energia, cor do marcador = laranja
			firePower = 2;
			setBulletColor(Color.ORANGE);
		} else {
			// Botão 1 ou botão desconhecido:
			// poder de fogo = 1 ponto de energia, cor do marcador = amarelo
			firePower = 1;
			setBulletColor(Color.YELLOW);
		}
	}

	// Chamado quando um botão do mouse é liberado (após ser pressionado)
	public void onMouseReleased(MouseEvent e) {
		// Fire power = 0, que significa "não dispare"
		firePower = 0;
	}

	// Chamado para pintar gráficos para este robô.
	// O botão "Paint" na janela do console do robô para este robô deve ser
	// habilitado para ver as pinturas.
	public void onPaint(Graphics2D g) {
		// Desenhe uma cruz vermelha com o centro no objetivo atual
		// coordenar (x, y)
		g.setColor(Color.RED);
		g.drawOval(aimX - 15, aimY - 15, 30, 30);
		g.drawLine(aimX, aimY - 4, aimX, aimY + 4);
		g.drawLine(aimX - 4, aimY, aimX + 4, aimY);
	}
}